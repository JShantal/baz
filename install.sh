#!/usr/bin/env bash

set -e

reset="\033[0m"
red="\033[31m"
green="\033[32m"
yellow="\033[33m"
cyan="\033[36m"
blue="\033[34m"
white="\033[37m"

printf "$green> Check chmod for storage directory$reset\n"
chmod -R 777 ./site/storage
chmod -R 777 ./users/storage

printf "$green> Shutdown all current containers$reset\n"
docker-compose down -v

printf "$green> Docker compose up$reset\n"
docker-compose up -d --build

printf "$green> Updating composer dependencies$reset\n"
docker exec -i site-php sh -c "composer update"

printf "$green> Running migrations$reset\n"
docker exec -i users-php sh -c "phalcon migration run"
