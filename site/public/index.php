<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Phalcon\Mvc\Micro;
use Phalcon\Di\FactoryDefault;

define('CONFIG_PATH', '../app/config/');
$config = include(CONFIG_PATH . 'config.php');
$di = new FactoryDefault();
include CONFIG_PATH . 'services.php';
include CONFIG_PATH . 'loader.php';

$app = new Micro();
include CONFIG_PATH . 'routes.php';

$app->handle();
