<?php

namespace App\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;

class LoginForm extends Form
{
    public function initialize()
    {
        $this->setEntity($this);

        $login = new Text('login');
        $login->setLabel('Имя пользователя')
            ->setFilters(['string', 'trim'])
            ->addValidator(new PresenceOf(['message' => 'Имя пользователя не может быть пустым']));

        $password = new Password('password');
        $password->setLabel('Пароль')
            ->setFilters(['string'])
            ->addValidator(new PresenceOf(['message' => 'Пароль не может быть пустым']))
            ->clear();

        $this->add($login);
        $this->add($password);
    }

    public function csrf()
    {
        echo "<input type='hidden' name='{$this->security->getTokenKey()}' value='{$this->security->getToken()}'/>";
    }

    public function renderDecorated($name)
    {
        $element = $this->get($name);
        $attributes = ['class' => 'form-control', 'placeholder' => $element->getLabel()];
        $messages = $this->getMessagesFor($element->getName());
        $errors = '';
        if (count($messages)) {
            $errors .= '<div class="invalid-feedback">';
            foreach ($messages as $message) {
                $errors .= $message->getMessage();
            }
            $errors .= '</div>';
            $attributes['class'] .= ' is-invalid';
        }
        $element->setAttributes($attributes);
        echo '<div class="form-group">';
        echo '<label for="', $element->getName(), '">', $element->getLabel(), '</label>';
        echo $element;
        echo $errors;
        echo '</div>';
    }

}
