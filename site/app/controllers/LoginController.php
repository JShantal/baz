<?php

namespace App\Controllers;

use App\Forms\LoginForm;

class LoginController extends Controller
{
    public function showLoginFormAction()
    {
        return $this->view();
    }

    public function loginAction()
    {
        if (!$this->security->checkToken()) {
            $this->response->setStatusCode(419, 'Page Expired');
            $this->flashSession->error('Вы отправили данные с устаревшей страницы. Попробуйте, пожалуйста, еще раз!');
            return $this->view();
        }

        $request = $this->request->getPost();
        $form = new LoginForm();
        if (!$form->isValid($request)) {
            $this->response->setStatusCode(422);
            return $this->view($form);
        }

        $response = $this->postService('users', 'login', $request);
        if ($response === false) {
            return $this->response;
        }
        if ($response['valid']) {
            $this->flashSession->success('Успешная авторизация');
        } else {
            $this->flashSession->error('Неверный логин или пароль');
        }

        return $this->view();
    }


    protected function view(?LoginForm $form = null)
    {
        $form = $form ?? new LoginForm();
        $view = $this->view->render('login', compact('form'));
        return $this->response->setContent($view);
    }
}
