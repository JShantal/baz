<?php


namespace App\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Phalcon\Mvc\Controller as BaseController;

class Controller extends BaseController
{
    protected function postService(string $service, string $url, array $payload = [])
    {
        $client = new Client([
            'base_uri' => "https://$service",
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'verify' => false,
            'exceptions' => false
        ]);
        $response = $client->post($url, [
            RequestOptions::JSON => $payload
        ]);
        return $this->proceedErrors($response);
    }

    protected function proceedErrors(Response $response)
    {
        $contents = $response->getBody()->getContents();
        $contents = $this->tryJson($contents);
        $statusCode = $response->getStatusCode();
        if ($statusCode < 300) {
            return $contents;
        }
        $this->setErrorResponse($statusCode, $contents);

        return false;
    }

    protected function setErrorResponse(int $code, $contents): void
    {
        $this->response->setStatusCode($code);
        if (is_array($contents)) {
            $this->response->setJsonContent($contents);
        } else {
            $this->response->setContent($contents);
        }
    }

    protected function tryJson(string $json)
    {
        $result = json_decode($json, true);
        if (json_last_error() === JSON_ERROR_NONE) {
            return $result;
        }

        return $json;
    }
}
