<?php

$loader = new Phalcon\Loader();
$loader->registerNamespaces([
    'App' => $config->application->libraryDir,
    'App\Controllers' => $config->application->controllersDir,
    'App\Models' => $config->application->modelsDir,
]);
$loader->register();
