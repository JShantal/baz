<?php

use App\Controllers\LoginController;
use Phalcon\Mvc\Micro\Collection as MicroCollection;

$auth = new MicroCollection();
$auth->setHandler(LoginController::class, true);
$auth->get('/', 'showLoginFormAction');
$auth->post('/', 'loginAction');
$app->mount($auth);

$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, 'Not Found')->sendHeaders();
    echo 'Not Found!';
});
