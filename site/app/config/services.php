<?php

use Phalcon\Flash\Session as Flash;
use Phalcon\Mvc\View\Simple as View;
use Phalcon\Session\Adapter\Files as SessionAdapter;

$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});

$di->setShared('view', function () {
    $view = new View();
    $view->setViewsDir($this->getConfig()->application->viewsDir);

    return $view;
});

$di->setShared('flashSession', function () {
    return new Flash([
        'error' => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice' => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ]);
});

$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

