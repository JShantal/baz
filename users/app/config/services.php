<?php

use Phalcon\Db\Adapter\Pdo\Sqlite;

$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});

$di->set('db', function () use ($config) {
    return new Sqlite($config->database->toArray());
});

