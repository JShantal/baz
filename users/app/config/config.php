<?php

defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: dirname(dirname(__DIR__)));
defined('APP_PATH') || define('APP_PATH', getenv('APP_PATH') ?: BASE_PATH . '/app');

use Phalcon\Config;

return new Config([
    'database' => [
        'adapter' => 'Sqlite',
        'dbname' => BASE_PATH . '/storage/database/database.db'
    ],
    'application' => [
        'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir' => APP_PATH . '/models/',
        'formsDir' => APP_PATH . '/forms/',
        'viewsDir' => APP_PATH . '/views/',
        'libraryDir' => APP_PATH . '/library/',
        'pluginsDir' => APP_PATH . '/plugins/',
        'cacheDir' => BASE_PATH . '/storage/cache',
        'migrationsDir' => APP_PATH . '/migrations/',
        'baseUri' => '/',
        'cryptSalt' => 'eEAfR|_&G&f,+vU]:jFr!!A&+71w1Ms9~8_4L!<@[N@DyaIP_2My|:+.u>/6m,$D'
    ]
]);
