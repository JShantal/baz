<?php

use App\Controllers\AuthController;
use Phalcon\Mvc\Micro\Collection as MicroCollection;

$auth = new MicroCollection();
$auth->setHandler(AuthController::class, true);
$auth->post('/login', 'loginAction');
$app->mount($auth);

$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, 'Not Found')->sendHeaders();
    echo 'Not Found!';
});
