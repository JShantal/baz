<?php

use Phalcon\Db\Index;
use Phalcon\Db\Column;
use Phalcon\Mvc\Model\Migration;

class UsersMigration_100 extends Migration
{
    public function morph()
    {
        $this->morphTable('users', [
            'columns' => [
                new Column('id', [
                    'type' => Column::TYPE_INTEGER,
                    'size' => 10,
                    'unsigned' => true,
                    'notNull' => true,
                    'autoIncrement' => true,
                    'first' => true,
                ]),
                new Column('login', [
                    'type' => Column::TYPE_VARCHAR,
                    'size' => 70,
                    'notNull' => true,
                    'after' => 'id',
                ]),
                new Column('password', [
                    'type' => Column::TYPE_VARCHAR,
                    'size' => 255,
                    'notNull' => true,
                    'after' => 'login',
                ]),
            ],
            'indexes' => [
                new Index('PRIMARY', ['id'], 'PRIMARY'),
                new Index('users_login_uindex', ['login'], 'UNIQUE'),
            ]
        ]);
    }

    public function up()
    {
        self::$connection->insert(
            'users',
            [
                null,
                'admin',
                '$2y$08$STMvYksrRVBlMDlkV3Q2W.jEll/mx06iy9E6Ng9JzCrNdlmMLOYk.' //'admin' hash
            ]
        );
    }

    public function down()
    {
        self::$connection->dropTable('users');
    }
}
