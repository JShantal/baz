<?php

namespace App\Controllers;

use App\Models\Users;
use Phalcon\Mvc\Controller;
use App\Validation\AuthRequest;

class AuthController extends Controller
{
    public function loginAction()
    {
        $request = $this->request->getJsonRawBody(true);
        if (!$this->validate($request)) {
            return $this->response;
        }
        $valid = $this->checkCredentials($request['login'], $request['password']);

        return $this->response->setJsonContent(compact('valid'));
    }

    protected function validate(array $request)
    {
        $validation = new AuthRequest();
        $messages = $validation->validate($request);
        if (!count($messages)) {
            return true;
        }
        $errors = [];
        foreach ($messages as $message) {
            $field = $message->getField();
            $errors[$field] = $errors[$field] ?? [];
            $errors[$field][] = $message->getMessage();
            $errors[$field] = array_unique($errors[$field]);
        }
        $this->response->setStatusCode(422, 'Validation exception');
        $this->response->setJsonContent(compact('errors'));

        return false;
    }

    protected function checkCredentials(string $login, string $password): bool
    {
        $user = Users::findFirstByLogin($login);
        if ($user) {
            return $this->security->checkHash($password, $user->password);
        }

        return false;
    }
}
